import './module';
import angular from 'angular';

angular.element(document).ready(function(){
    //angular.bootstrap(document,['productRegistrationWebApp.module'], {strictDi: true});
    angular.bootstrap(document,['productRegistrationWebApp.module']);
});